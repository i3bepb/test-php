<?php

$this->pageTitle = 'Просмотр товара - ' . $product['name'];
?>

<div class="container product-full">
    <div class="product-image">
        <img src="<?php echo $product['image_url']; ?>"/>
    </div>
    <div class="product-data">
        <h3><?php echo $product['name']; ?></h3>
        <p>Цена:
            <?php if($product['inet_price'] < $product['old_price']) : ?>
                <span class="old-price"><?php echo $product['old_price'] ?></span>
            <?php endif; ?>
            <span><?php echo $product['inet_price'] ?></span>
        </p>
        <p><?php echo $product['description']; ?></p>
    </div>
</div>

<?php if(!empty($comments)) : ?>
<div class="container">
    <h3>Комментарии</h3>
    <?php foreach($comments as $comment) : ?>
        <div>
            <h4><?php echo $comment['name']; ?></h4>
            <p><?php echo $comment['comment']?></p>
        </div>
    <?php endforeach; ?>
</div>
<?php endif; ?>

<form action="<?php echo $this->createUrl('review',['product_id' => $product['id']]) ?>" method="post">
    <div class="container comment-form form">
        <h3>Добавить комментарий</h3>
        <div class="row">
            <label for="name">Имя</label>
            <input name="name" id="input-name" type="text" />
            <p class="hint">Ведите ваше имя</p>
        </div>
        <div class="row">
            <label for="email">Email</label>
            <input name="email" id="input-email" type="text" />
            <p class="hint">Ведите адрес вашей электронной почты</p>
        </div>
        <div class="row">
            <label for="text">Текст</label>
            <textarea name="text" id="input-text"></textarea>
            <p class="hint">Ведите ваш комментарий</p>
        </div>
        <div class="row">
            <input name="comment" id="input-comment" type="submit" value="Отправить" />
        </div>
    </div>
</form>