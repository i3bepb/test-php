<?php

class SiteController extends Controller
{

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex($page = 0)
	{
        $products = Yii::app()->db->createCommand(
            "SELECT * FROM product LIMIT 10 OFFSET ".($page * 10)
        )->queryAll();

        $count = Yii::app()->db->createCommand(
            "SELECT COUNT(*) FROM product"
        )->queryScalar();


		$this->render('index',['products' => $products, 'count' => $count]);
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

    public function actionProduct($id = 0)
    {
        if($id === 0) {
            return false;
        }

        $product = Yii::app()->db->createCommand(
            "SELECT * FROM product WHERE id=".$id
        )->queryRow();
        $comments = Yii::app()->db->createCommand(
            "SELECT * FROM review WHERE product_id=".$id
        )->queryAll();

        $this->render('product', ['product' => $product, 'comments' => $comments]);
    }

	/**
	 * Displays the contact page
	 */
	public function actionReview()
	{
        Yii::app()->db->createCommand(
            "INSERT INTO review (product_id, name, email, comment) VALUES (".
            $_GET['product_id'].", '".$_POST['name']."', '".$_POST['email']."', '".$_POST['text']."')"
        )->execute();

        $this->redirect($this->createUrl('product', ['id' => $_GET['product_id']]));
	}
}